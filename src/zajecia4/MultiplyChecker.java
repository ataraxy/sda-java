package zajecia4;

import java.util.Random;
import java.util.Scanner;

public class MultiplyChecker {
    public static void main(String[] args) {
        boolean czyPoprawnaOdpowiedz = false;
        System.out.println("Losuja sie dwie liczby... Podaj wynik ich mnozenia.");
        Random rd = new Random();
        Scanner sc = new Scanner(System.in);
        int liczba1 = rd.nextInt(10);
        int liczba2 = rd.nextInt(10);

        System.out.println(liczba1);
        System.out.println(liczba2);

        while (czyPoprawnaOdpowiedz == false) {
            System.out.println("Podaj wynik mnozenia " + liczba1 + " * " + liczba2 + ':');
            int wynik = sc.nextInt();
            if (wynik == liczba1 * liczba2) {
                System.out.println("Poprawna odpowiedz.");
                czyPoprawnaOdpowiedz = true;
            } else {
                System.out.println("Bledna odpowiedz.");
            }

        }

    }
}
