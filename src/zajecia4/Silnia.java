package zajecia4;

// Napisz program obliczajacy silnie liczby naturalnej

import java.util.Scanner;

public class Silnia {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        long n = sc.nextInt();
        long wynik = 1;

        //obliczanie silni
        wynik = MathHelper.factorial(n);
        System.out.println("Wynik to: " + wynik);

    }
}
