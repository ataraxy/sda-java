package zajecia4;

public class ArrayIntro {
    public static void main(String[] args) {
        // Utworzenie tablicy 10-elementowej
        int[] tab = new int[10];

//        tab[0] = 25;
//        tab[1] = 20;
//        tab[2] = 50;

        // Wypelnic tablice elementami, takimi ze w kazdym indeksie
        // jest wartosc + 20
        // 20, 21, 22, 23, 24...

        // petla wypelniajaca wartosciami
        for (int i = 0; i < tab.length; i++) {
            tab[i] = i + 20;
        }

        // petla drukujaca wartosci
        for (int i = 0; i < tab.length; i++) {
            System.out.println("Indeks: " + i + " wartosc: " + tab[i]);
        }

        // policzyc sume elementow:
        int suma = 0;
        for (int i = 0; i < tab.length; i++) {
            int elem = tab[i];
            suma += elem;
//            suma += tab[i];
        }

        System.out.println("Suma elementow tablicy to: " + suma);

        // policzyc srednia:
        System.out.println("Srednia: " + ((double)suma / tab.length));
        }
    }

