package zajecia2;

import java.util.Scanner;

/*  Program pobierajacy dwie liczby calkowite i obliczajacy wyrazenie logiczne
    (a < b) && (a != b)
 */

public class OperatoryLogiczneIntro {
    public static void main(String[] args) {
        int a;
        int b;

        // inicjalizacja obiektu typu skaner
        System.out.println("Podaj wartość wyrażenia \'a\': ");
        Scanner odczyt = new Scanner(System.in);
        a = odczyt.nextInt();

        System.out.println("Podaj wartosc wyrazenia \'b\': ");
        b = odczyt.nextInt();

        // obliczam wyrazenie
        boolean wynik = (a >= b) && (a != b);
        System.out.println("Wyrazenie jest " + wynik);
    }
}
