package zajecia2;

import java.util.Scanner;

public class Konwerter {
    public static void main(String[] args) {
        double temperatura;
        double stopnie;

        System.out.println("Podaj temperature w stopniach Celsjusza: ");
        Scanner odczyt = new Scanner(System.in);
        stopnie = odczyt.nextDouble();

        temperatura = 1.8 * stopnie + 32.0;

        System.out.println("Temperatura w stopniach Fahrenheita = " + temperatura);
    }
}
