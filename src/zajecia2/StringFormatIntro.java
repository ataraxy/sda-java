package zajecia2;

public class StringFormatIntro {
    public static void main(String[] args) {

        String imie = "Piotr";
        String nazwisko = "Siwinski";
        int wiek = 18;
        double wzrost = 180.7385;

        System.out.println("Masz na imie " + imie + ", a nazywasz sie " + nazwisko + ".");

        String komunikat = String.format("Masz na imie %s, a nazywasz sie %s i masz %d lat i masz %.2f cm wzrostu.", imie, nazwisko, wiek, wzrost);
        System.out.println(komunikat);
    }
}
