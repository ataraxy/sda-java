package zajecia2.instrukcjesterujace;

/*
    Dla podanych dwoch liczb z klawiatury
    wypisuje wieksza z nich.
    Zakladamy, ze nie podajemy dwoch takich samych
    A != B
 */

import java.util.Scanner;

public class FindGreaterNumber {
    public static void main(String[] args){

        int a;
        int b;
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj dwie liczby całkowite a != b: ");
        a = odczyt.nextInt();
        b = odczyt.nextInt();

        if(a > b){
            System.out.println("Wieksza jest liczba a: " + a);
        } else if (a < b){
            System.out.println("Wieksza jest liczba b: " + b);
        } else {
            System.out.println("Liczby są równe.");
        }

    }
}
