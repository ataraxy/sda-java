package zajecia2;

/*
Napisac program, ktory oblicza wielkosc BMI (waga / wzrost * 2)
< 18.5  -> niedowaga
<18.5; 24.9> -> ok
(24.9 ; 29> - nadwaga
*/

import java.util.Scanner;

public class ObliczanieBMI {
    public static void main(String[] args) {
        float bmi;
        float wzrostWMetrach;
        float wagaWKg;
        Scanner odczyt = new Scanner(System.in);

        System.out.println("Podaj wzrost w metrach: ");
        wzrostWMetrach = odczyt.nextFloat();

        System.out.println("Podaj wage w kilogramach: ");
        wagaWKg = odczyt.nextFloat();

        bmi = wagaWKg / (wzrostWMetrach * wzrostWMetrach);

        System.out.println("Wartość BMI wynosi: " + bmi + ". ");

        if (bmi < 18.5) {
            System.out.println("Niedowaga.");
        } else if ((bmi >= 18.5) && (bmi < 25)) {
            System.out.println("Prawidłowa waga.");
        } else {
            System.out.println("Nadwaga");
        }

    }
}
