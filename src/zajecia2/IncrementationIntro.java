package zajecia2;

public class IncrementationIntro {
    public static void main(String[] args) {
        int wiek = 10;
        // trzy przyklady zwiekszania o 1
        wiek = wiek + 1;
        wiek += 1;
        wiek++;

        int wzrost = 150;

        wzrost = wzrost + 20;
        wzrost += 20;

    }
}
