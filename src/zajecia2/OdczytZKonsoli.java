package zajecia2;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

/*
    Program pobierajacy imie, nazwisko i wiek
    i drukujace te informacje na konsoli
 */

public class OdczytZKonsoli {
    public static void main(String[] args){

        // zapytaj uzytkownika o imie
        String imie; // dobra praktyka jest deklarowanie/inicjalizowanie zmiennych na gorze
        String nazwisko;
        int wiek;

        System.out.println("Jak masz na imie?");

        // utworzenie obiektu typu Scanner o nazwie odczyt
        Scanner odczyt = new Scanner(System.in);

        // do zmiennej imie wstaw nastepna linie z konsoli
        imie = odczyt.nextLine();

        // odczytaj z konsoli nazwisko
        System.out.println("Podaj nazwisko: ");
        nazwisko = odczyt.nextLine();

        //odczytaj z konsoli liczbe i wstaw do zmiennej
        System.out.println("Podaj wiek: ");
        wiek = odczyt.nextInt();

        System.out.println("Witaj, " + imie + " " + nazwisko + "! " + "Masz " + wiek + " lat.");
}
}
