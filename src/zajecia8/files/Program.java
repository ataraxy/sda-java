package zajecia8.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Program {
    public static void main(String[] args) /* throws FileNotFoundException */ {

        // feature z Javy 6 - stary sposób otwierania i zamykania pliku

        System.out.println("Zawartosc pliku: ");

        String line;

        BufferedReader reader = null;
        FileReader fileReader = null;

        try {
            fileReader = new FileReader("notatka.txt");
            reader = new BufferedReader(fileReader);
//            System.out.println(reader.readLine());
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
