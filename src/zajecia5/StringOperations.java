package zajecia5;

import java.util.Scanner;

public class StringOperations {
    public static void main(String[] args) {
        // wczytaj od uzytkownika napis do 20 znakow
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj napis do 20 znakow: ");
        String text = sc.nextLine();

        // sprawdzamy czy text ma ponizej 20 znakow, jesli tak to wykonujemy program
        if (text.length() < 20) {
            printMenu();
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Opcja 1:");
                    toUpper(text);
                    break;
                case 2:
                    System.out.println("Opcja 2:");
                    toLower(text);
                    break;
                case 3:
                    System.out.println("Opcja 3:");
                    toggleCase(text);
                    break;
            }
        } else {
            System.out.println("Napis jest zbyt dlugi.");
        }


        // zamien wszystkie litery na duze

        // zamien wszystkie litery na male

        // zamien duze litery na male, a male na duze
    }

    public static void printMenu() {
        System.out.println("1. Wszystkie litery na duze.");
        System.out.println("2. Wszystkie litery na male.");
        System.out.println("3. Wszystkie male na duze, a duze na male.");
        System.out.println("Twoj wybor: ");
    }

    public static void toUpper(String input) {
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            // pobierz znak na i-tej pozycji
            char element = input.charAt(i);

            // jezeli jest to mala litera to zwieksz
            if (element >= 97 && element <= 122) {
                element -= 32;
            }

            result += element;

            System.out.print(element);

        }
    }

    public static void toLower(String str) {
//        String result = "";
//        for (int i = 0; i < input.length(); i++) {
//            char element = input.charAt(i);
//
//            // jezeli jest to duza litera to zmniejsz
//            if (element >= 65 && element <= 90) {
//                element += 32;
//            }
//
//            result += element;
//
//            System.out.print(element);
    }


    public static void toggleCase(String str) {
//        String result = "";

//        for (int i = 0; i < str.length(); i++) {
//            char element = str.charAt(i);
//
//            // jezeli jest to duza litera to zmniejsz, a jesli mala to zwieksz
//            if (element >= 97 && element <= 122) {
//                element -= 32;
//            } else if (element >= 65 && element <= 90) {
//                element += 32;
//            }
//
//            result += element;
//
//            System.out.print(element);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char element = str.charAt(i);
            if (element >= 65 && element <= 90) {
                element += 32;
            } else if (element >= 97 && element <= 122) {
                element -= 32;
            }
            sb.append(element);
        }
        String result = sb.toString();
        System.out.println(result);
        }
    }

