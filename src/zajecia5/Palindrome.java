package zajecia5;

import java.util.Scanner;

public class Palindrome {

    public static boolean isPalindrome(String text) {
        text = text.replace(" ", "").toLowerCase();
        for (int i = 0; i < text.length() / 2; i++) {
            if (text.charAt(i) != text.charAt(text.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("Podaj napis - sprawdze czy to palindrom:");
        Scanner sc = new Scanner(System.in);
        String text = sc.nextLine();
        System.out.println(isPalindrome(text) ? "Jest to palindrom." : "To nie jest palindrom.");
    }
}
