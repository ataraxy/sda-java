package zajecia5;

import java.util.Random;
import java.util.Scanner;

public class SearchInArray {
    public static void main(String[] args) {
        // utworzyc tablice 30 elementow
        int[] tablica = new int[30];
        Random rd = new Random();


        // wypelnic losowymi wartosciami
        for (int i = 0; i < tablica.length; i++) {
            // I - bardziej czytelny sposob
            int pomocnicza = rd.nextInt(100);
            tablica[i] = pomocnicza;

            // II - mniej czytelny sposob - zaawansowany
            // tablica[i] = rd.nextInt(100);
        }
        // uzytkownik wprowadza jakas liczbe
        Scanner sc = new Scanner(System.in);

        // sprawdzamy czy taka liczba wystepuje w tablicy
        int liczba = 0;
        System.out.println("Podaj jakas liczbe z zakresu od 0 do 100: ");
        liczba = sc.nextInt();

        // wypisz elementy tablicy
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }
        System.out.println();

        boolean czyObecna = false;

        // jesli liczba istnieje w tablicy to wypisz na konsoli, ze istnieje
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == liczba) {
                czyObecna = true;
                break;
            }
        }

//        System.out.println("Wynik wyszukiwania: " + czyObecna);

        boolean wynik = ArrayHelper.isPresent(tablica, liczba);

        if (wynik) {
            System.out.println("Element wystepuje w tablicy.");
        } else {
            System.out.println("Element nie wystepuje w tablicy.");
        }

        // operator trojargumentowy
        // trueorfalse ? jesli_true_to_wykonaj_to_polecenie : jesli_false_to_wykonaj_polecenie_po_dwukropku
        String napis = wynik ? "Element w tablicy" : "Nie ma takiego";
        System.out.println(napis);

    }
}
