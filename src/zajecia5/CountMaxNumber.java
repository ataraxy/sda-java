package zajecia5;

import java.util.Random;
import java.util.Scanner;

public class CountMaxNumber {
    public static void main(String[] args) {


        // wypelnic losowymi wartosciami n-liczb
        Random rd = new Random(); // jesli wpiszemy wartosc w new Random();
        int arraySize = rd.nextInt(30) + 1;
        int[] array = new int[arraySize];

        for (int i = 0; i < arraySize; i++) {
            array[i] = rd.nextInt(20) + 1;
        }

        for (int i = 0; i < arraySize; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        // znalezc max
        int maxNumber = array[0];
        for (int i = 1; i < arraySize; i++) {
            if (array[i] > maxNumber) {
                maxNumber = array[i];
            }
        }
        System.out.println("Najwieksza wartosc to: " + maxNumber);

        int countMaxNumber = 0;
        for (int i = 0; i < arraySize; i++) {
            if (array[i] == maxNumber) {
                countMaxNumber += 1;
            }
        }

        // zliczyc ile razy max sie pojawia
        System.out.println("Najwieksza wartosc wystepuje " + countMaxNumber + " razy.");

    }
}