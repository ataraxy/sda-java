package zajecia7.abstraction;

public class Kolo extends Figura {
    public double getR() {
        return r;
    }

    public Kolo(double r) {
        this.r = r;
    }


    private double r;

    @Override
    public double obliczPole() {
        System.out.println("Pole kola: ");

        return Math.PI * (r * r);
    }

    @Override
    public double obliczObwod() {
        System.out.println("Obwod kola: ");

        return 2 * Math.PI * r;
    }

}
