package zajecia7.abstraction.pr;

import zajecia7.abstraction.Figura;
import zajecia7.abstraction.Kolo;
import zajecia7.abstraction.Kwadrat;
import zajecia7.abstraction.Prostokat;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Figura[] tablicaFigur = new Figura[10];
        int ilosc = 0;
        boolean czyWyjsc = false;
        while (!czyWyjsc) {
            System.out.println("Dodaj nowa figure");
            System.out.println("1. Kwadrat");
            System.out.println("2. Prostokat");
            System.out.println("3. Kolo");
            System.out.println("4. Pola wszystkich");
            System.out.println("5. Wyjscie");
            int wybor = scanner.nextInt();
            Figura figura;
            switch (wybor) {
                case 1: {
                    System.out.println("Podaj bok kwadratu");
                    int bok = scanner.nextInt();
                    figura = new Kwadrat(bok);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 2: {
                    System.out.println("Podaj boki prostokata");
                    int bok1 = scanner.nextInt();
                    int bok2 = scanner.nextInt();
                    figura = new Prostokat(bok1, bok2);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 3: {
                    System.out.println("Podaj promien kola");
                    double promien = scanner.nextDouble();
                    figura = new Kolo(promien);
                    tablicaFigur[ilosc++] = figura;
                    System.out.println(figura.obliczPole());
                    System.out.println(figura.obliczObwod());
                    break;
                }
                case 4: {
                    System.out.println("Pola wszystkich");
                    for (int i = 0; i < ilosc; i++) {
                        Figura f = tablicaFigur[i];
                        System.out.println(f.obliczPole());
                        System.out.println();
                    }
                    break;
                }
                case 5: {
                    czyWyjsc = true;
                    break;
                }

            }
        }

    }
}
