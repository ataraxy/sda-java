package zajecia7.abstraction;

public class Prostokat extends Figura {
    private double a;
    private double b;

    public Prostokat(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    @Override
    public double obliczPole() {
        System.out.println("pole prostokota");
        return a * b;
    }

    @Override
    public double obliczObwod() {
        return 2 * a + 2 * b;
    }
}
