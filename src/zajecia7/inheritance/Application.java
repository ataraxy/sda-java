package zajecia7.inheritance;

public class Application {
    public static void main(String[] args) {
        Person osoba1 = new Person("Jan", "Kowalski", 26);
        System.out.println(osoba1.describe());

        Student student1 = new Student("Adam", "Nowacki", 57);
        System.out.println(student1.describe());

        Student studentInformatyki = new Student("Kasia", "Pawlak", 22, "123456", "Politechnika");
        System.out.println(studentInformatyki.describe());

        Worker pracownik1 = new Worker("Bob", "Budowniczy", 36, "murarz", 5000);
        System.out.println(pracownik1);
    }
}
