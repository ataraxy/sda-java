package zajecia7.inheritance;

//    Zaimplementuj klasy Person do reprezentacji osoby (z polami name,surname oraz age) oraz
//    klasy Student (z polami numer indeksu, kierunek, uczelnia), Pracownik (pola pensja,
//    stanowisko) oraz Pacjent (z polem typu tablica obiektow typu String).
//    Utworz kilka obiektow kazdego z tych typow i wywołaj ich metody

public class Student extends Person {

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    private String index;
    String major;
    String university;

    public Student(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Student(String name, String surname, int age, String index, String university) {
        super(name, surname, age);
        this.index = index;
        this.university = university;
    }

    @Override // <-- adnotacja
    public String describe() {
        // dopisywanie czegos do poprzedniej metody
        String opis = super.describe();
        return String.format("%s i jestem studentem %s", opis, getUniversity());
        //return String.format("%s %s i jestem studentem %s", getName(), getSurname(), getUniversity());
    }

    @Override
    public String toString() {
        return String.format("%s %s i jestem studentem %s", getName(), getSurname(), getUniversity());
    }
}
