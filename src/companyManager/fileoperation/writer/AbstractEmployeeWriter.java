package companyManager.fileoperation.writer;

import companyManager.Employee;


public abstract class AbstractEmployeeWriter implements EmployeeWriter {

    protected String pathToFile;

    protected AbstractEmployeeWriter(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    @Override
    public abstract void writeEmployees(Employee[] employees);
}
