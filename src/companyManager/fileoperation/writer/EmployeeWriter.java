package companyManager.fileoperation.writer;

import companyManager.Employee;

public interface EmployeeWriter {
    void writeEmployees (Employee[] employees);
}
