package companyManager;

import companyManager.fileoperation.EmployeeReaderFactory;
import companyManager.fileoperation.reader.EmployeeReader;
import companyManager.fileoperation.reader.JsonEmployeeReader;
import companyManager.fileoperation.reader.XmlEmployeeReader;
import companyManager.fileoperation.util.ArrayUtil;
import companyManager.fileoperation.writer.EmployeeWriter;
import companyManager.fileoperation.writer.XmlEmployeeWriter;

import java.util.Arrays;
import java.util.Scanner;

public class Application {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Witaj w programie: ");
        System.out.println("Podaj nazwe firmy: ");
        String name = sc.nextLine();
        Company myCompany = new Company(name);

        boolean exit = false;
        while (!exit) {
            printMenu();
            int wybor = sc.nextInt();
            switch (wybor) {
                case 1:
                    add(myCompany);
                    print(myCompany);
                    break;
                case 2:
                    print(myCompany);
                    break;
                case 3:
                    importEmployees(myCompany);
                    break;
                case 4:
                    saveEmployees(myCompany);
                    break;

                default:
                    break;
            }
        }

    }

    private static void saveEmployees(Company myCompany) {
        System.out.println("Podaj nazwe pliku z rozszerzeniem: ");
        String pathToFile = scanner.nextLine();
        EmployeeWriter writer = new XmlEmployeeWriter(pathToFile);
        int size = ArrayUtil.countElements(myCompany.getEmployees());
        Employee[] copy = Arrays.copyOf(myCompany.getEmployees(), size);
        writer.writeEmployees(copy);
    }

    public static void importEmployees(Company myCompany) {
        System.out.println("Podaj sciezke do pliku: ");
        String pathToFile = scanner.nextLine();
        EmployeeReader reader = EmployeeReaderFactory.createReader(pathToFile);
        Employee[] employees = reader.readEmployees();
        myCompany.addEmployees(employees);
    }

    public static void print(Company myCompany) {
        for (Employee e : myCompany.getEmployees()) {
            if (e != null) {
                System.out.println(e.getDescription());
            }
        }
    }

    public static void add(Company myCompany) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj imie: ");

        String empName = scanner.nextLine();
        System.out.println("Podaj nazwisko: ");
        String empSurname = scanner.nextLine();
        System.out.println("Podaj pensje");
        double salary = scanner.nextDouble();

        Employee employee = new Employee(empName, empSurname, salary);
        boolean isSuccess = myCompany.addEmployee(employee);
        if (isSuccess) {
            System.out.println("Dodano pracownika");
        } else {
            System.out.println("Nie udalo sie dodac - zbyt duzo pracownikow");
        }
    }

    public static void printMenu() {
        System.out.println("Wybierz opcje: ");
        System.out.println("1. Dodawanie pracownika: ");
        System.out.println("2. Wyswietlanie pracownikow: ");
        System.out.println("3. Import pracownikow");
        System.out.println("4. Zapisz pracownikow do pliku");
        System.out.println("Wybor: ");
    }
}
