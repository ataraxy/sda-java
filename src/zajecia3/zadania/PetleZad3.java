package zajecia3.zadania;

//          Napisz program, który przy użyciu instrukcji for sumuje
//          liczby całkowite od 1 do 100.

public class PetleZad3 {

    public static void main(String[] args) {
        int suma = 0;
        for (int i = 1; i < 101; i++) {
            suma += i;
        }
        System.out.println(suma);
    }

}
