package zajecia3;

public class PostIncrementationExample {
    public static void main(String[] args) {

        int licznik = 0;
        System.out.println(++licznik); //preinkrementacja (licznik ++ oznacza: licznik = licznik + 1;
        System.out.println(licznik++); //postinkrementacja
    }
}
