package zadania.instrukcje_warunkowe;

import java.util.Scanner;

public class PierwiastekRownaniaKwadratowego {

    public static void main(String[] args) {
        double xOne;
        double xTwo;
        double xThree;
        double a;
        double b;
        double c;

        Scanner wstawWartosc = new Scanner(System.in);

        System.out.println("Podaj wartość \'a\': ");
        a = wstawWartosc.nextDouble();

        System.out.println("Podaj wartość \'b\': ");
        b = wstawWartosc.nextDouble();

        System.out.println("Podaj wartość \'c\': ");
        c = wstawWartosc.nextDouble();

        double bKwadrat = Math.pow(b, 2);

        double delta = bKwadrat - (4 * a * c);

        System.out.println("Delta wynosi: " + delta);

        double pierwiastekDelta = Math.sqrt(delta);

        System.out.println("Pierwiastek z delty wynosi: " + pierwiastekDelta);

        System.out.println(delta);

        if (delta > 0) {
            xOne = (-b - pierwiastekDelta) / (2 * a);
            xTwo = (-b + pierwiastekDelta) / (2 * a);
            System.out.println("Równanie ma dwa rozwiązania: " + xOne + ", " + xTwo);

        } else if (delta == 0) {
            xThree = (-b) / (2 * a);
            System.out.println("Równanie ma jedno rozwiązanie: " + xThree);
        } else {
            System.out.println("Równanie nie ma rozwiązań.");
        }

    }

}
