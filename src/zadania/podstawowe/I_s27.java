package zadania.podstawowe;

public class I_s27 {



    public static void main(String[] args){
//      typ nazwa   =   wartość;
        int liczba1;    // deklaracja
        liczba1 = 0;    // inicjalizacja
        short liczba2 = 123;
        long liczba3 = 4422123;
        double liczba4 = 2.23;
        float liczba5 =  2.401f;

        final int i = 10;
//        i = 12;

        System.out.println("Zmienna \"i\" równa się: " + i);


        String wyraz = "asd";

        char znak = 'c';
        boolean testLogiczny = 0<1;

        // Wypisujemy w konsoli podane zdania/wartości:
        System.out.println("Hello world!");
        System.out.println("Ania");
        System.out.println("Bartek");
        System.out.println("Kasia");
        System.out.println();

        //  Zadanie s27
        System.out.println(liczba1);
        System.out.println(liczba2);
        System.out.println(liczba3);
        System.out.println(liczba4);
        System.out.println(liczba5);
        System.out.println(wyraz);
        System.out.println(znak);
        System.out.println(testLogiczny);
        System.out.println();

        String mojeImie = "Jarek";
        System.out.println(mojeImie);


    }
}
