package zadania.podstawowe;

public class I_s28WynikDzielenia {
    public static void main(String[] args) {
        int zmiennaInt = 17;
        double zmiennaDouble = 3.17;
        String zmiennaString = "dom";
        char zmiennaChar = 'x';

        String a = "A";
        String b = "B";
        String c = "C";
        String abc = a + b + c;


        System.out.println(zmiennaInt);
        System.out.println(zmiennaDouble);
        System.out.println(zmiennaString);
        System.out.println(zmiennaChar);

        zmiennaInt = 18;

        double x = 7d;
        double y = 3;

        double wynikDzielenia = x / y;

        System.out.println("Wynik dzielenia to: " + wynikDzielenia);

        System.out.println(zmiennaInt);

        System.out.println();

        System.out.println(abc);

    }
}
