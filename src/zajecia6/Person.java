package zajecia6;

/**
 * Klasa Person do reprezentacji danych o osobie
 */

public class Person {
    // stan - mowi o tym jaka ta osoba jest - przechowuje jej dane
    public String name;
    public String surname;
    public int age;
    public String email;
    private boolean registered;

    // 1 kontruktor
    public Person (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    // 2 konstruktor
    public Person (String name, String surname, int age) {
        this(name, surname); // wywolanie pierwszego konstruktora
        this.age = age;
    }

    // 3 konsruktor
    public Person (String name, String surname, int age, String email) {
        this(name, surname, age); // wywolanie drugiego konstruktora
        this.email = email;
    }


    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    // zachowanie - jakas czynnosc
    public void sayHello() {
        System.out.println("Jestem " + name + " " + surname + ".");
    }


}
