package zajecia6.zadanie1;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        // Stworzyc 3 obiekty typu prostokat i wywolac jego metody
        Scanner scanner = new Scanner(System.in);

        Rectangle pr1 = new Rectangle();
        System.out.println("Podaj pierwszy bok prostokata");
//        pr1.a = scanner.nextInt();
        pr1.setA(scanner.nextInt());
        System.out.println("Podaj drugi bok prostokata");
        pr1.setB(scanner.nextInt());

        Rectangle pr2 = new Rectangle();
        System.out.println("Podaj pierwszy bok prostokata");
//        pr2.a = scanner.nextInt();
        pr2.setA(scanner.nextInt());
        System.out.println("Podaj drugi bok prostokata");
        pr2.setB(scanner.nextInt());

        Rectangle pr3 = new Rectangle();
//        pr3.a = 5;
      System.out.println("Podaj pierwszy bok prostokata");
        pr3.setA(scanner.nextInt());
        System.out.println("Podaj drugi bok prostokata");
        pr3.setB(scanner.nextInt());

        System.out.println("Obwody prostokatow: ");
        pr1.circumference();
        System.out.println(pr1.circumference());
        pr2.circumference();
        System.out.println(pr2.circumference());
        pr3.circumference();
        System.out.println(pr3.circumference());

        System.out.println("Pola prostokatow: ");
        pr1.field();
        System.out.println(pr1.field() + " j2");
        pr2.field();
        System.out.println(pr2.field() + " j2");
        pr3.field();
        System.out.println(pr3.field() + " j2");
    }
}
